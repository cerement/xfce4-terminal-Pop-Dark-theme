# Pop-Dark theme for xfce4-terminal

[![Casual Maintenance Intended](https://casuallymaintained.tech/badge.svg)](https://casuallymaintained.tech/)

Pop-Dark color theme ported from Gnome Terminal to xfce4-terminal

copy `pop-dark.theme` into `~/.local/share/xfce4/terminal/colorschemes/`

original source: [pop-os/desktop/debian/pop-desktop.gsettings-override](https://github.com/pop-os/desktop/blob/733a5e202fa154b18f427daad1000c46eaa2c597/debian/pop-desktop.gsettings-override)
